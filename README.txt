To be able to install nodegit, we need an up-to-date version of openssl.

$ brew install openssl 
$ brew link openssl --force

See https://github.com/nodegit/nodegit/issues/728