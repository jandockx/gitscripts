#!/usr/bin/env node

var path = require("path");
var NodeGit = require("nodegit");
var argv = require("yargs")
  .strict()
  .version(function () {
    return require("../package").version;
  })
  .usage("Usage: $0 [-v|-vv] oldName newName")
  //.command(
  //  "renameRemoteBranch",
  //  "Rename a remote branch. Checkout the remote branch, rename it locally, push it,"
  //  + " delete the old remote branch, delete the local branch.")
  .example(
    "$0 -v branchName feature/branchName", "Rename the remote branch with name \"branchName\" to"
    + " \"feature/branchName\", and show what the command is doing.")
  .demand(
    2,
    2,
    "Two arguments required: the first argument is the old remote branch name, "
    + "the second the new remote branch name.")
  .option(
    "remote",
    {
      alias: "r",
      string: true,
      describe: "Name of the remote to work in. Mandatory if there is more then 1 remote defined "
                + " in the repository. Otherwise, the remote must match the one defined."
    }
  )
  .option(
    "verbose",
    {
      alias: "v",
      count: true,
      describe: "v show more detail about what the command is doing, vv shows even more"
    })
  .help("h")
  .alias("h", "help")
  .epilog("Copyright 2015")
  .argv;





function warn() {
  argv.verbose >= 0 && console.log.apply(console, arguments);
}
function info() {
  argv.verbose >= 1 && console.log.apply(console, arguments);
}
function debug() {
  argv.verbose >= 2 && console.log.apply(console, arguments);
}





var cwd = path.resolve(process.cwd());

info("git repository: " + cwd);

var opened = NodeGit.Repository
  .open(cwd)
  .catch(function(reasonForFailure) {
    warn(reasonForFailure);
  });
var inNoneState = opened.then(function(repo) {
  var state = repo.state();
  debug("Repo opened: " + repo.path() + "; status: " + state);
  if (state !== NodeGit.Repository.STATE.NONE) {
    throw "Cannot operate. Repository is in state " + state + ".";
  }
  return repo;
});
var remotesGotten = inNoneState
  .then(function(repo) {
    return repo.getRemotes();
  })
  .catch(function(exc) {
    warn("Failed to determine remotes.");
    throw exc;
  });
remotesGotten.then(function(remotes) {
  debug("Remotes: " + remotes);
});
var remoteDetermined = remotesGotten
  .then(function(remotes) {
    if (!remotes || remotes.length <= 0) {
      throw "No remotes found in repository " + cwd + ". Cannot rename remote branches without remotes.";
    }
    if (argv.remote && remotes.indexOf(argv.remote) < 0) {
      throw "Remote " + argv.remote + " not defined in repository " + cwd + ". Found only " + remotes.join(", ") + ".";
    }
    if (remotes.length > 1 && !argv.remote) {
      throw "Found more than one remote in repository " + cwd + ", and no remote defined in arguments. "
      + "Please specify one of -r " + remotes.join(", -r ") + ".";
    }
    return argv.remote || remotes[0];
  })
  .catch(function(exc) {
    warn("Failed to determine remote.");
    warn(exc);
    throw exc;
  });
remoteDetermined.then(function(remote) {
    debug("remote: " + remote);
  });
var referencesGotten = opened
  .then(function(repo) {
    return repo.getReferences(NodeGit.Reference.TYPE.LISTALL);
  })
  .catch(function(exc) {
    warn("Failed to get remote branches.");
    warn(exc);
    throw exc;
  });
referencesGotten.then(function(references) {
  debug("references: " + references);
});
var remoteBranchesDetermined = referencesGotten
  .then(function(references) {
    return references.filter(function(r) {return r.isRemote() && r.isBranch();}); // TODO doesn't work?
  })
  .catch(function(exc) {
    warn(exc);
    throw exc;
  });
remoteBranchesDetermined.then(function(remoteBranches) {
  debug("remote branches: " + remoteBranches);
});

console.log("old name: " + argv._[0]);
console.log("new name: " + argv._[1]);

console.log("Hello, world");

warn("Showing only important stuff");
info("Showing semi-important stuff too");
debug("Extra chatty mode");
